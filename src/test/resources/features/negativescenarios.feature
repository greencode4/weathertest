Feature: Testing Negative scenarios

  Scenario Outline:Verify Weather api without API Key
    Given user does not have an api key
    When users gets weather for a "<location>"
    Then  API should return error
    Examples:
      | location |
      | New York |

  Scenario Outline:Verify Weather api with invalid city
    Given user has valid api key
    When users gets weather for a invalid "<location>"
    Then API should return valid response
    And Response content type should be application json
    Examples:
      | location |
      | Invalid  |

  Scenario Outline:Verify Weather api with invalid unit
    Given user has valid api key
    When users gets weather for a "<location>" and "<unit>"
    Then API should return valid response
    And Response content type should be application json
    Examples:
      | location | unit    |
      | London   | invalid |