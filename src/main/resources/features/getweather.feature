Feature: Get Weather for location

  Scenario Outline:Verify Weather api with city
    Given user has valid api key
    When users gets weather for a "<location>"
    Then API should return valid 200 response
    And Response content type should be application json
    Examples:
      | location |
      | New York |
      | London   |

  Scenario Outline:Verify Weather api with different units
    Given user has valid api key
    When users gets weather for a "<location>" and "<unit>"
    Then API should return valid 200 response
    And Response content type should be application json
    Examples:
      | location | unit |
      | London   | m    |
      | Berlin   | m    |

  Scenario Outline:Verify temperature is returned by API
    Given user has valid api key
    When users gets weather for a "<location>" and "<unit>"
    Then API should return valid 200 response
    And Response content type should be application json

    Examples:
      | location | unit |
      | London   | m    |
      | NewYork  | m    |


