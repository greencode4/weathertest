package actions;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

import constants.Constants;
import constants.ErrorCodes;
import io.restassured.response.Response;
import java.util.Map;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;


public class InValidWeatherActions {
  public static Response res;
  public static Response invalidRes;

  @Step
  public static void setInvalidKey() {
    invalidRes = SerenityRest.get(Constants.INVALIDBASEURL);
    System.out.println(invalidRes);
    Map<String, String> result = invalidRes.jsonPath().getMap("error");
    System.out.println(invalidRes.jsonPath().getMap("error"));
    assertThat(result.get("code"), equalTo(ErrorCodes.INVALDACCESSKEYERROR));
  }

  @Step
  public void userHasInvalidApiKey() {
    invalidRes = SerenityRest.get(Constants.INVALIDBASEURL);
    Map<String, String> result = invalidRes.jsonPath().getMap("error");
    System.out.println(invalidRes.jsonPath().getMap("error"));
    assertThat(result.get("code"), equalTo(ErrorCodes.INVALDACCESSKEYERROR));
  }

  @Step
  public void setInValidLocation(String city) {
    res = SerenityRest.get(Constants.BASEURL + "&query=" + city);
    System.out.println(Constants.BASEURL + "&query=" + city);
    System.out.println(res.jsonPath().getString("success"));
    Map<String, Integer> x = res.jsonPath().getMap("error");
    System.out.println(x.get("code"));
    assertThat(x.get("code"), equalTo(ErrorCodes.REQUESTFAILEDCODE));
  }
}
