package actions;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

import constants.Constants;
import constants.ErrorCodes;
import io.restassured.response.Response;
import java.util.Map;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;
import org.hamcrest.core.IsNull;

@SuppressWarnings("checkstyle:MissingJavadocType")
public class WeatherActions {

  public static Response res;
  public static Response invalidRes;
  
  @Step
  public void requestWeatherWithGet(String city) {
    res = SerenityRest.get(Constants.BASEURL + "&query=" + city);
    System.out.println(Constants.BASEURL + "&query=" + city);
    System.out.println(res.jsonPath().getString("request"));
    Map<String, String> x = res.jsonPath().getMap("request");
    System.out.println(x.get("type"));
    assertThat(x.get("type"), equalTo("City"));
    assertThat(x.get("language"), equalTo("en"));
  }

  @Step
  public void requestWeatherWithUnit(String location, String unit) {
    res = SerenityRest.get(Constants.BASEURL + "&query=" + unit + "&query=" + location);
    System.out.println(Constants.BASEURL);
    System.out.println(res.jsonPath().getString("request"));
    Map<String, String> x = res.jsonPath().getMap("request");
    System.out.println(x.get("type"));
    Map<String, Integer> x2 = res.jsonPath().getMap("current");
    System.out.println(x2.get("temperature"));
    assertThat(x2.get("temperature"), IsNull.notNullValue());
  }

  @Step
  public void requestWeatherWithUnitAndLanguage(String location, String language) {
    res = SerenityRest.get(Constants.BASEURL + "&query=" + location + "&query=" + language);
    System.out.println(Constants.BASEURL);
    System.out.println(res.jsonPath().getString("request"));
    Map<String, String> x = res.jsonPath().getMap("request");
    System.out.println(x.get("type"));
  }

  @Step
  public void apiShouldReturnError() {
    invalidRes = SerenityRest.get(Constants.INVALIDBASEURL);
    System.out.println(invalidRes);
    Map<String, String> result = invalidRes.jsonPath().getMap("error");
    assertThat(result.get("code"), equalTo(ErrorCodes.INVALDACCESSKEYERROR));
  }

  @Step
  public int getStatusCode() {
    return res.getStatusCode();
  }

  @Step
  public String getContentType() {
    return res.getContentType();
  }
}
