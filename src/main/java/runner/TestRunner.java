package runner;

import io.cucumber.junit.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
    plugin = {"pretty"},
    features = {"src/test/resources/features/getWeather.feature",
      "src/test/resources/features/negativeScenarios.feature"},
    glue = "steps"
)

public class TestRunner {
		
}
