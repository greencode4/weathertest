package steps;

import actions.InValidWeatherActions;
import actions.WeatherActions;
import constants.Constants;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;

public class GetWeatherSteps {
  @Steps
  WeatherActions weatherAct;
  String weatherApiKey;

  @Steps
  InValidWeatherActions inValidWeatherAct;

  @Given("user has valid api key")
  public void userHasValidApiKey() {
    weatherApiKey = Constants.ACCESSKEY;
  }

  @When("users gets weather for a {string}")
  public void usersGetsWeatherFora(String city) {
    weatherAct.requestWeatherWithGet(city);
  }

  @Then("API should return valid response")
    public void apiShouldReturnValidResponse() {
    weatherAct.getStatusCode();
  }

  @Then("API should return invalid {int} response")
  public void apiShouldReturnInvalidResponse() {
    weatherAct.getStatusCode();
  }

  @And("Response content type should be application json")
  public void responseContentTypeShouldBeApplicationJson() {
    weatherAct.getContentType();
  }

  @When("users gets weather for a {string} and {string}")
  public void usersGetsWeatherForCity(String location, String unit) {
    weatherAct.requestWeatherWithUnit(location, unit);
  }

  @When("users requests weather with {string} and {string}")
  public void usersRequestsWeatherWithLocationAndLanguage(String location, String language) {
    weatherAct.requestWeatherWithUnitAndLanguage(location, language);
  }

  @Then("API should return error")
  public void apiShouldReturnError() {
    weatherAct.apiShouldReturnError();
  }

  @Given("user has invalid api key")
  public void userHasInvalidApiKey() {
    inValidWeatherAct.userHasInvalidApiKey();
  }

  @Given("user does not have an api key")
  public void userDoesNotHaveAnApiKey() {
    InValidWeatherActions.setInvalidKey();
  }

  @When("users gets weather for a invalid {string}")
  public void usersGetsWeatherForInvalidCity(String city) {
    inValidWeatherAct.setInValidLocation(city);
  }
}
