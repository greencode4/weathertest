package constants;

public class Constants {
  public static final String ACCESSKEY = "35f2aeb0caae2d09ae42bf2d1023e049";
  public static final String BASEURL = "http://api.weatherstack.com/current?access_key=" + ACCESSKEY;
  public static final String INVALIDBASEURL = "http://api.weatherstack.com/current?access_key=";
}
