Weather API Tests
Weather API can be used to get current and historical weather for cities in the world.
Please find steps to write a new test in Installation section.

Installation
Clone the project using
git@gitlab.com:greencode4/weathertest.git
or
https://gitlab.com/greencode4/weathertest.git
OR
Download attached project and import as maven project in IntelliJ Idea

Writing Tests:

Add a new feature file to features package with the required gherkin steps
Add the endpoint to Constants java file in Constants package
Implement cucumber steps by adding step definitions to Steps package
Add the corresponding actions class in Action package and call get or put request

Executing tests:

mvn compile
mvn verify

OR

Tests can also be run in CI/CD pipeline on GITLAB

Reports:
HTML Test Reports are generated at the location:/target/site/serenity/index.html
